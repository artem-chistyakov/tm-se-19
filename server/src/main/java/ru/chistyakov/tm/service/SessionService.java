package ru.chistyakov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.chistyakov.tm.dto.SessionDTO;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.enumerate.RoleType;
import ru.chistyakov.tm.repository.ISessionRepository;
import ru.chistyakov.tm.repository.IUserRepository;
import ru.chistyakov.tm.utility.SignatureUtil;

@Service
@NoArgsConstructor
public final class SessionService implements ru.chistyakov.tm.api.ISessionService {
    @Autowired
    private ISessionRepository sessionRepository;

    @Autowired
    private PropertiesService propertiesService;

    @Autowired
    private IUserRepository userRepository;


    @Override
    public void validate(@Nullable final SessionDTO session) throws IllegalAccessException {
        if (session == null) throw new IllegalAccessException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new IllegalAccessException();
        if (session.getTimestamp() == null) throw new IllegalAccessException();
        if (session.getSignature() == null || session.getSignature().isEmpty())
            throw new IllegalAccessException();
        final String signatureSource = session.getSignature();
        final SessionDTO sessionClone = (SessionDTO) session.clone();
        sessionClone.setSignature(null);
        if (propertiesService.getSALT() == null || propertiesService.getCycle() == null)
            throw new IllegalArgumentException("Ошибка загрузки проперти файла");
        final String signatureTarget = SignatureUtil.sign(sessionClone, propertiesService.getSALT(), propertiesService.getCycle());
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new IllegalAccessException();
        if (!sessionRepository.existsById(session.getId())) throw new IllegalAccessException();

    }

    @Override
    public boolean isValid(@Nullable final SessionDTO session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    @Transactional
    public SessionDTO open(@Nullable final String userId, @Nullable final RoleType roleType) {
        if (userId == null || roleType == null) return null;
        final SessionDTO session = new SessionDTO();
        session.setUserId(userId);
        session.setRoleType(roleType);
        session.setSignature(SignatureUtil.sign(session, propertiesService.getSALT(), propertiesService.getCycle()));
        sessionRepository.save(mapSessionDtoToSession(session));
        return session;
    }

    @Override
    @Transactional
    public boolean close(Session session) {
        if (session == null) return false;
        sessionRepository.delete(session);
        return true;
    }

    private Session mapSessionDtoToSession(@NotNull final SessionDTO sessionDTO) {
        final Session session = new Session();
        session.setId(sessionDTO.getId());
        session.setSignature(sessionDTO.getSignature());
        session.setTimestamp(sessionDTO.getTimestamp());
        session.setRoleType(sessionDTO.getRoleType());
        session.setUser(userRepository.findById(sessionDTO.getUserId()).get());
        return session;
    }
}
