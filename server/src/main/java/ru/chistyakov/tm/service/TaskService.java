package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.chistyakov.tm.dto.TaskDTO;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.repository.IProjectRepository;
import ru.chistyakov.tm.repository.ITaskRepository;
import ru.chistyakov.tm.repository.IUserRepository;

import java.util.Collection;
import java.util.List;

@Service
public final class TaskService implements ru.chistyakov.tm.api.ITaskService {

    @Autowired
    private ITaskRepository taskRepository;

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private IUserRepository userRepository;

    @Override
    @Transactional
    public Task merge(@Nullable final Task task) {
        if (task == null) return null;
        return taskRepository.save(task);
    }

    @Override
    @Transactional
    public Task persist(@Nullable final Task task) {
        if (task == null) return null;
        return taskRepository.save(task);
    }

    @Override
    public @Nullable Task findOne(@Nullable final String id) {
        if (id == null) return null;
        return taskRepository.findById(id).get();
    }

    @Override
    public @Nullable Collection<Task> findAllByUserId(@Nullable final String userId) {
        if (userId == null) return null;
        return taskRepository.findByUserId(userId);
    }

    @Override
    @Transactional
    public void remove(@Nullable final String id) {
        if (id == null) return;
        taskRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null) return;
        taskRepository.deleteByUserId(userId);
    }

    @Override
    public @Nullable List<Task> findAllInOrderDateBegin(@Nullable final String userId) {
        if (userId == null) return null;
        return taskRepository.findByUserIdOrderByDateBeginTaskAsc(userId);
    }

    @Override
    public @Nullable List<Task> findAllInOrderDateEnd(@Nullable final String userId) {
        if (userId == null) return null;
        return taskRepository.findByUserIdOrderByDateEndTaskAsc(userId);
    }

    @Override
    public @Nullable List<Task> findAllInOrderReadinessStatus(@Nullable final String userId) {
        if (userId == null) return null;
        return taskRepository.findByUserIdOrderByReadinessStatusAsc(userId);
    }

    @Override
    public @Nullable List<Task> findByPartNameOrDescription(@Nullable final String userId, @Nullable final String part) {
        if (userId == null) return null;
        return taskRepository.findByUserIdAndNameLikeOrDescriptionLikeIgnoreCase(userId,part,part);
    }

    @Override
    public @Nullable List<Task> findAll() {
        return taskRepository.findAll();
    }

    public @NotNull Task mapDtoToTask(final @NotNull TaskDTO taskDTO) {
        final Task task = new Task();
        task.setId(taskDTO.getId());
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setDateBeginTask(taskDTO.getDateBeginTask());
        task.setDateEndTask(taskDTO.getDateEndTask());
        task.setUser(userRepository.findById(taskDTO.getUserId()).get());
        task.setProject(projectRepository.findById(taskDTO.getProjectId()).get());
        task.setReadinessStatus(taskDTO.getReadinessStatus());
        return task;
    }

    public @NotNull TaskDTO mapTaskToDto(final @NotNull Task task) {
        final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setDateBeginTask(task.getDateBeginTask());
        taskDTO.setDateEndTask(task.getDateEndTask());
        taskDTO.setUserId(task.getUser().getId());
        taskDTO.setProjectId(task.getProject().getId());
        taskDTO.setReadinessStatus(taskDTO.getReadinessStatus());
        return taskDTO;
    }
}
