package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.repository.IProjectRepository;
import ru.chistyakov.tm.repository.ITaskRepository;
import ru.chistyakov.tm.repository.IUserRepository;

import java.util.List;

@Service
public final class ProjectService implements ru.chistyakov.tm.api.IProjectService {

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private ITaskRepository taskRepository;

    @Autowired
    private IUserRepository userRepository;

    @Override
    @Transactional
    public Project merge(@Nullable final Project project) {
        if (project == null) return null;
        return projectRepository.save(project);
    }

    @Override
    @Transactional
    public Project persist(@Nullable final Project project) {
        if (project == null) return null;
        return projectRepository.save(project);
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null) return;
        projectRepository.deleteByUserId(userId);
    }

    @Override
    @Transactional
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        projectRepository.deleteById(id);
    }

    @Override
    public @Nullable Project findOne(@Nullable final String id) {
        if (id == null) return null;
        return projectRepository.findById(id).get();
    }

    @Override
    public @Nullable List<Project> findAllByUserId(@Nullable final String userId) {
        if (userId == null) return null;
        return projectRepository.findByUserId(userId);
    }

    @Override
    public @Nullable List<Project> findAllInOrderDateBegin(@Nullable final String userId) {
        if (userId == null) return null;
        return projectRepository.findByUserIdOrderByDateBeginProjectAsc(userId);
    }

    @Override
    public @Nullable List<Project> findAllInOrderDateEnd(@Nullable final String userId) {
        if (userId == null) return null;
        return projectRepository.findByUserIdOrderByDateEndProjectAsc(userId);
    }

    @Override
    public @Nullable List<Project> findAllInOrderReadinessStatus(@Nullable final String userId) {
        if (userId == null) return null;
        return projectRepository.findByUserIdOrderByReadinessStatusAsc(userId);
    }

    @Override
    public @Nullable List<Project> findByPartNameOrDescription(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || part == null) return null;
        return projectRepository.findByUserIdAndNameLikeOrDescriptionLike(userId, part, part);

    }

    @Override
    public @Nullable List<Project> findAll() {
        return projectRepository.findAll();
    }

    public @NotNull  Project mapDtoToProject(final @NotNull ProjectDTO projectDTO) {
        final Project project = new Project();
        project.setId(projectDTO.getId());
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setDateBeginProject(projectDTO.getDateBeginProject());
        project.setDateEndProject(projectDTO.getDateEndProject());
        project.setUser(userRepository.findById(projectDTO.getUserId()).get());
        project.setTasks(taskRepository.findByProjectId(projectDTO.getId()));
        project.setReadinessStatus(projectDTO.getReadinessStatus());
        return project;
    }

    public @NotNull  ProjectDTO mapProjectToDto(final @NotNull Project project) {
        final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setDateBeginProject(project.getDateBeginProject());
        projectDTO.setDateEndProject(projectDTO.getDateEndProject());
        projectDTO.setUserId(project.getUser().getId());
        projectDTO.setReadinessStatus(project.getReadinessStatus());
        return projectDTO;
    }
}