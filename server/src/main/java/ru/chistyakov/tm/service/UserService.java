package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.chistyakov.tm.dto.UserDTO;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.repository.IProjectRepository;
import ru.chistyakov.tm.repository.ITaskRepository;
import ru.chistyakov.tm.repository.IUserRepository;
import ru.chistyakov.tm.utility.PasswordParser;

import java.util.Collection;

@Service
public final class UserService implements ru.chistyakov.tm.api.IUserService {

    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private ITaskRepository taskRepository;
    @Autowired
    private IProjectRepository projectRepository;


    @Override
    public @Nullable User authoriseUser(@Nullable final String login, @Nullable final String password) {
        if (login == null || password == null || login.isEmpty() || password.isEmpty()) return null;
        final String passwordMD5 = PasswordParser.parse(password);
        return userRepository.findUserByLoginAndPassword(login, passwordMD5);
    }

    @Override
    @Transactional
    public boolean updateUser(@Nullable final User user) {
        if (user == null) return false;
        return userRepository.updateUser(user.getId(), user.getLogin(), user.getPassword()) > 0;
    }

    @Override
    @Transactional
    public User registryUser(@Nullable final User user) {
        return userRepository.save(user);
    }


    @Override
    public @Nullable User findById(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return userRepository.findById(userId).get();
    }

    public @Nullable User findByLoginAndPassword(@NotNull final String login,@NotNull final String password){
        final String passwordMd5 = PasswordParser.parse(password);
        return userRepository.findUserByLoginAndPassword(login,password);
    }
    @Override
    public void deleteAccount(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) return;
        userRepository.deleteById(userId);
    }

    @Override
    public @Nullable Collection<User> findAll() {
        return userRepository.findAll();
    }

    public @NotNull User mapUserDtoToUser(@NotNull final UserDTO userDTO) {
        final User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPassword(userDTO.getPassword());
        user.setRoleType(userDTO.getRoleType());
        user.setTasks(taskRepository.findByUserId(userDTO.getId()));
        user.setProjects(projectRepository.findByUserId(userDTO.getId()));
        return user;
    }

    public @NotNull UserDTO mapUserToUserDto(@NotNull final User user) {
        final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPassword(user.getPassword());
        userDTO.setRoleType(user.getRoleType());
        return userDTO;
    }
}
