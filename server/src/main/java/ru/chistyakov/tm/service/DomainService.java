package ru.chistyakov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.chistyakov.tm.api.IProjectService;
import ru.chistyakov.tm.api.ITaskService;
import ru.chistyakov.tm.api.IUserService;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.dto.TaskDTO;
import ru.chistyakov.tm.dto.UserDTO;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.repository.IProjectRepository;
import ru.chistyakov.tm.repository.ITaskRepository;
import ru.chistyakov.tm.repository.IUserRepository;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@Service
public class DomainService implements ru.chistyakov.tm.api.IDomainService {

    @Autowired
    private IProjectRepository projectRepository;
    @Autowired
    private ITaskRepository taskRepository;
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private IProjectService projectService;
    @Autowired
    private ITaskService taskService;
    @Autowired
    private IUserService userService;

    @Override
    public void serializateDomain() {
        try (@NotNull final FileOutputStream projectFileOutputStream = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "projectFile.txt");
             @NotNull final FileOutputStream taskFileOutputStream = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "taskFile.txt");
             @NotNull final FileOutputStream userFileOutputStream = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "userFile.txt")
        ) {
            final List<ProjectDTO> projectDTOList = new ArrayList<>();
            for (Project project : projectRepository.findAll())
                projectDTOList.add(projectService.mapProjectToDto(project));
            final List<TaskDTO> taskDTOList = new ArrayList<>();
            for (Task task : taskRepository.findAll()) taskDTOList.add(taskService.mapTaskToDto(task));
            final List<UserDTO> userDTOList = new ArrayList<>();
            for (User user : userRepository.findAll()) userDTOList.add(userService.mapUserToUserDto(user));
            final ObjectOutputStream projectObjectOutputStream = new ObjectOutputStream(projectFileOutputStream);
            projectObjectOutputStream.writeObject(projectDTOList);
            final ObjectOutputStream taskObjectOutputStream = new ObjectOutputStream(taskFileOutputStream);
            taskObjectOutputStream.writeObject(taskDTOList);
            final ObjectOutputStream userObjectOutputStream = new ObjectOutputStream(userFileOutputStream);
            userObjectOutputStream.writeObject(userDTOList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserializateDomain() {
        try (final FileInputStream userFileInputStream = new FileInputStream(new File(".").getAbsolutePath() + File.separator + "projectFile.txt");
             final FileInputStream projectFileInputStream = new FileInputStream(new File(".").getAbsolutePath() + File.separator + "taskFile.txt");
             final FileInputStream taskFileInputStream = new FileInputStream(new File(".").getAbsolutePath() + File.separator + "userFile.txt")
        ) {
            final ObjectInputStream userObjectInputStream = new ObjectInputStream(userFileInputStream);
            final ObjectInputStream projectObjectInputStream = new ObjectInputStream(projectFileInputStream);
            final ObjectInputStream taskObjectInputStream = new ObjectInputStream(taskFileInputStream);
            final Collection<UserDTO> userDTOCollection = (Collection<UserDTO>) userObjectInputStream.readObject();
            for (final UserDTO userDTO : userDTOCollection) {
                userRepository.save(userService.mapUserDtoToUser(userDTO));
            }
            final Collection<ProjectDTO> projectDTOCollection = (Collection<ProjectDTO>) projectObjectInputStream.readObject();
            for (final ProjectDTO projectDTO : projectDTOCollection) {
                projectRepository.save(projectService.mapDtoToProject(projectDTO));
            }
            final Collection<TaskDTO> taskDTOCollection = (Collection<TaskDTO>) taskObjectInputStream.readObject();
            for (TaskDTO taskDTO : taskDTOCollection) {
                taskRepository.save(taskService.mapDtoToTask(taskDTO));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveDomainJacksonXml() {
        try (FileOutputStream fOSProject = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "projectXml.xml");
             FileOutputStream fOSTask = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "taskXml.xml");
             FileOutputStream fOSUser = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "userXml.xml")
        ) {
            final JacksonXmlModule module = new JacksonXmlModule();
            module.setDefaultUseWrapper(false);
            final XmlMapper xmlMapper = new XmlMapper(module);
            final List<ProjectDTO> projectDTOList = new ArrayList<>();
            for (Project project : projectRepository.findAll())
                projectDTOList.add(projectService.mapProjectToDto(project));
            final List<TaskDTO> taskDTOList = new ArrayList<>();
            for (Task task : taskRepository.findAll()) taskDTOList.add(taskService.mapTaskToDto(task));
            final List<UserDTO> userDTOList = new ArrayList<>();
            for (User user : userRepository.findAll()) userDTOList.add(userService.mapUserToUserDto(user));
            final String projectJsonArray = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(projectDTOList);
            final String taskJsonArray = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(taskDTOList);
            final String userJsonArray = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(userDTOList);
            fOSProject.write(projectJsonArray.getBytes());
            fOSProject.flush();
            fOSTask.write(taskJsonArray.getBytes());
            fOSTask.flush();
            fOSUser.write(userJsonArray.getBytes());
            fOSUser.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadDomainJacksonXml() {
        try {
            final JacksonXmlModule jacksonXmlModule = new JacksonXmlModule();
            jacksonXmlModule.setDefaultUseWrapper(false);
            final XmlMapper xmlMapper = new XmlMapper(jacksonXmlModule);
            final Iterator<ProjectDTO> projectIterator = xmlMapper.readerFor(ProjectDTO.class).readValues(new File(".").getAbsolutePath() + File.separator + "projectXml.xml");
            final Iterator<TaskDTO> taskIterator = xmlMapper.readerFor(TaskDTO.class).readValues(new File(".").getAbsolutePath() + File.separator + "taskXml.xml");
            final Iterator<UserDTO> userIterator = xmlMapper.readerFor(UserDTO.class).readValues(new File(".").getAbsolutePath() + File.separator + "userXml.xml");
            while (userIterator.hasNext()) {
                userRepository.save(userService.mapUserDtoToUser(userIterator.next()));
            }
            while (projectIterator.hasNext()) {
                projectRepository.save(projectService.mapDtoToProject(projectIterator.next()));
            }
            while (taskIterator.hasNext()) {
                taskRepository.save(taskService.mapDtoToTask(taskIterator.next()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveDomainJacksonJson() {
        try (FileOutputStream fOSProject = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "projectJson.json");
             FileOutputStream fOSTask = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "taskJson.json");
             FileOutputStream fOSUser = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "userJson.json")
        ) {
            final ObjectMapper objectMapper = new ObjectMapper();
            final List<ProjectDTO> projectDTOList = new ArrayList<>();
            for (Project project : projectRepository.findAll())
                projectDTOList.add(projectService.mapProjectToDto(project));
            final List<TaskDTO> taskDTOList = new ArrayList<>();
            for (Task task : taskRepository.findAll()) taskDTOList.add(taskService.mapTaskToDto(task));
            final List<UserDTO> userDTOList = new ArrayList<>();
            for (User user : userRepository.findAll()) userDTOList.add(userService.mapUserToUserDto(user));
            final String projectJsonArray = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(projectDTOList);
            final String taskJsonArray = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(taskDTOList);
            final String userJsonArray = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(userDTOList);
            fOSProject.write(projectJsonArray.getBytes());
            fOSTask.write(taskJsonArray.getBytes());
            fOSUser.write(userJsonArray.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadDomainJacksonJson() {
        try {
            final ObjectMapper objectMapper = new ObjectMapper();
            final Iterator<ProjectDTO> projectIterator = objectMapper.readerFor(ProjectDTO.class).readValues(new File(".").getAbsolutePath() + File.separator + "projectJson.json");
            final Iterator<TaskDTO> taskIterator = objectMapper.readerFor(TaskDTO.class).readValues(new File(".").getAbsolutePath() + File.separator + "taskJson.json");
            final Iterator<UserDTO> userIterator = objectMapper.readerFor(UserDTO.class).readValues(new File(".").getAbsolutePath() + File.separator + "userJson.json");
            while (userIterator.hasNext()) {
                userRepository.save(userService.mapUserDtoToUser(userIterator.next()));
            }
            while (projectIterator.hasNext()) {
                projectRepository.save(projectService.mapDtoToProject(projectIterator.next()));
            }
            while (taskIterator.hasNext()) {
                taskRepository.save(taskService.mapDtoToTask(taskIterator.next()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}