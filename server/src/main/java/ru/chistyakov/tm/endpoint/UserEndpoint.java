package ru.chistyakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.IDomainService;
import ru.chistyakov.tm.api.ISessionService;
import ru.chistyakov.tm.api.IUserService;
import ru.chistyakov.tm.dto.SessionDTO;
import ru.chistyakov.tm.dto.UserDTO;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collection;

@WebService(endpointInterface = "ru.chistyakov.tm.api.IUserEndpoint")
@XmlRootElement
@Component
public final class UserEndpoint extends AbstractEndpoint implements ru.chistyakov.tm.api.IUserEndpoint {

    @NotNull
    @Autowired
    private IDomainService domainService;
    @NotNull
    @Autowired
    private ISessionService sessionService;
    @NotNull
    @Autowired
    private IUserService userService;

    public UserEndpoint() {
        this.URL = "http://localhost:8080/UserEndpoint?wsdl";
    }

    @Override
    @WebMethod
    public boolean registryUser(@Nullable @WebParam(name = "userDTO", partName = "userDTO") final UserDTO userDTO) {
        if (userDTO == null) return false;
        return userService.registryUser(userService.mapUserDtoToUser(userDTO)) != null;
    }

    @Override
    @WebMethod
    public SessionDTO authorizeUser(@Nullable @WebParam(name = "login", partName = "login") final String login,
                                    @Nullable @WebParam(name = "password", partName = "password") final String password) {
        if (login == null || password == null) return null;
        final User user = userService.authoriseUser(login, password);
        if (user == null) return null;
        else {
            return sessionService.open(user.getId(), user.getRoleType());
        }
    }

    @Override
    @WebMethod
    public boolean updateUser(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                              @NotNull @WebParam(name = "userDTO", partName = "userDTO") final UserDTO userDTO) throws IllegalAccessException {
        if (session == null) return false;
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        else return userService.updateUser(userService.mapUserDtoToUser(userDTO));
    }


    @Override
    @WebMethod
    @Nullable
    public UserDTO findUserById(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) throws IllegalAccessException {
        if (session == null) return null;
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        return userService.mapUserToUserDto(userService.findById(session.getUserId()));
    }

    @Override
    @WebMethod
    public void deleteAccount(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                              @Nullable @WebParam(name = "userId", partName = "userId") final String userId) throws IllegalAccessException {
        if (userId == null) return;
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        userService.deleteAccount(userId);
    }

    @Override
    @WebMethod
    @Nullable
    public UserDTO findWithLoginPassword(@Nullable SessionDTO session, @Nullable String login, @Nullable String password) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        return userService.mapUserToUserDto(userService.findByLoginAndPassword(login, password));
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<UserDTO> findAllUser(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        if(session.getRoleType()!= RoleType.ADMINISTRATOR) throw  new IllegalArgumentException("Вам не доступна данная команда");
        final Collection<UserDTO> collection = new ArrayList<>();
        for (final User user : userService.findAll())
            collection.add(userService.mapUserToUserDto(user));
        return collection;
    }

    @Override
    @WebMethod
    public void serializateDomain(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        if(session.getRoleType()!= RoleType.ADMINISTRATOR) throw  new IllegalArgumentException("Вам не доступна данная команда");
        domainService.serializateDomain();
    }

    @Override
    @WebMethod
    public void deserializateDomain(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        if(session.getRoleType()!= RoleType.ADMINISTRATOR) throw  new IllegalArgumentException("Вам не доступна данная команда");
        domainService.deserializateDomain();
    }

    @Override
    @WebMethod
    public void saveDomainJacksonXml(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        if(session.getRoleType()!= RoleType.ADMINISTRATOR) throw  new IllegalArgumentException("Вам не доступна данная команда");
        domainService.saveDomainJacksonXml();
    }

    @Override
    @WebMethod
    public void loadDomainJacksonXml(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        domainService.loadDomainJacksonXml();
    }

    @Override
    @WebMethod
    public void saveDomainJacksonJson(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        if(session.getRoleType()!= RoleType.ADMINISTRATOR) throw  new IllegalArgumentException("Вам не доступна данная команда");
        domainService.saveDomainJacksonJson();
    }

    @Override
    @WebMethod
    public void loadDomainJacksonJson(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        if(session.getRoleType()!= RoleType.ADMINISTRATOR) throw  new IllegalArgumentException("Вам не доступна данная команда");
        domainService.loadDomainJacksonJson();
    }
}
