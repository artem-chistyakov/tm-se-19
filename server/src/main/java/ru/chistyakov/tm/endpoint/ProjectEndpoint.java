package ru.chistyakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.IProjectService;
import ru.chistyakov.tm.api.ISessionService;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.dto.SessionDTO;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.enumerate.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collection;

@WebService(endpointInterface = "ru.chistyakov.tm.api.IProjectEndpoint")
@XmlRootElement
@Component
public final class ProjectEndpoint extends AbstractEndpoint implements ru.chistyakov.tm.api.IProjectEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;
    @NotNull
    @Autowired
    private IProjectService projectService;

    public ProjectEndpoint() {
        this.URL = "http://localhost:8080/ProjectEndpoint?wsdl";
    }

    @Override
    @WebMethod
    public boolean persistProject(@Nullable final @WebParam(name = "session", partName = "session") SessionDTO session,
                                  @Nullable final @WebParam(name = "project", partName = "project") ProjectDTO project) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        else return projectService.persist(projectService.mapDtoToProject(project)) != null;
    }

    @Override
    @WebMethod
    public boolean mergeProject(@Nullable final @WebParam(name = "session", partName = "session") SessionDTO session,
                                @Nullable final @WebParam(name = "project", partName = "project") ProjectDTO project) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        else return projectService.merge(projectService.mapDtoToProject(project)) != null;
    }

    @Override
    @WebMethod
    public @Nullable ProjectDTO findOneProject(@Nullable final @WebParam(name = "session", partName = "session") SessionDTO session,
                                               @Nullable final @WebParam(name = "projectId", partName = "projectId") String projectId) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        else return projectService.mapProjectToDto(projectService.findOne(projectId));
    }

    @Override
    @WebMethod
    public boolean removeProject(@Nullable final @WebParam(name = "session", partName = "session") SessionDTO session,
                                 @Nullable final @WebParam(name = "projectId", partName = "projectId") String projectId) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        else projectService.remove(projectId);
        return true;
    }

    @Override
    @WebMethod
    public boolean updateProject(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                                 @Nullable @WebParam(name = "project", partName = "project") final ProjectDTO project) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        else return false;
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<ProjectDTO> findAllProjectByUserId(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        final Collection<ProjectDTO> collection = new ArrayList<>();
        for (final Project project : projectService.findAllByUserId(session.getUserId())) {
            collection.add(projectService.mapProjectToDto(project));
        }
        return collection;
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<ProjectDTO> findAllProject(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) throws IllegalAccessException{
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидна") ;
        if(session.getRoleType()!=RoleType.ADMINISTRATOR) throw new IllegalArgumentException("Вам не доступна эта команда");
        final Collection<ProjectDTO> collection = new ArrayList<>();
        for (final Project project : projectService.findAll()) {
            collection.add(projectService.mapProjectToDto(project));
        }
        return collection;
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<ProjectDTO> findAllProjectInOrderDateBegin(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session)throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная") ;
        final Collection<ProjectDTO> collection = new ArrayList<>();
        for (final Project project : projectService.findAllInOrderDateBegin(session.getUserId())) {
            collection.add(projectService.mapProjectToDto(project));
        }
        return collection;
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<ProjectDTO> findAllProjectInOrderDateEnd(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        final Collection<ProjectDTO> collection = new ArrayList<>();
        for (final Project project : projectService.findAllInOrderDateEnd(session.getUserId())) {
            collection.add(projectService.mapProjectToDto(project));
        }
        return collection;
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<ProjectDTO> findAllProjectInOrderReadinessStatus(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session)
    throws IllegalAccessException{
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        final Collection<ProjectDTO> collection = new ArrayList<>();
        for (final Project project : projectService.findAllInOrderReadinessStatus(session.getUserId())) {
            collection.add(projectService.mapProjectToDto(project));
        }
        return collection;
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<ProjectDTO> findByPartProjectNameOrDescription(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                                                                     @Nullable @WebParam(name = "part", partName = "part") final String part)throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        final Collection<ProjectDTO> collection = new ArrayList<>();
        for (final Project project : projectService.findByPartNameOrDescription(session.getUserId(), part)) {
            collection.add(projectService.mapProjectToDto(project));
        }
        return collection;
    }

    @Override
    @WebMethod
    public void removeAllProject(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session)throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        if(session.getRoleType()!=RoleType.ADMINISTRATOR) throw new IllegalArgumentException("Вам не доступна эта команда");
        projectService.removeAll(session.getUserId());
    }
}