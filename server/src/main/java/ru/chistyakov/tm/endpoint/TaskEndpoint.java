package ru.chistyakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.ISessionService;
import ru.chistyakov.tm.api.ITaskService;
import ru.chistyakov.tm.dto.SessionDTO;
import ru.chistyakov.tm.dto.TaskDTO;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.enumerate.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collection;

@WebService(endpointInterface = "ru.chistyakov.tm.api.ITaskEndpoint")
@XmlRootElement
@Component
public final class TaskEndpoint extends AbstractEndpoint implements ru.chistyakov.tm.api.ITaskEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;
    @NotNull
    @Autowired
    private ITaskService taskService;

    public TaskEndpoint() {
        this.URL = "http://localhost:8080/TaskEndpoint?wsdl";
    }

    @Override
    @WebMethod
    public boolean persistTask(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                               @Nullable @WebParam(name = "task", partName = "task") final TaskDTO task) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        else return taskService.persist(taskService.mapDtoToTask(task)) != null;
    }

    @Override
    @WebMethod
    public boolean mergeTask(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                             @Nullable @WebParam(name = "task", partName = "task") final TaskDTO task) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        else return taskService.merge(taskService.mapDtoToTask(task)) != null;
    }

    @Override
    @WebMethod
    public boolean removeTask(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                              @Nullable @WebParam(name = "taskId", partName = "taskId") final String taskId) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        if (taskId == null) return false;
        taskService.remove(taskId);
        return true;
    }

    @Override
    @WebMethod
    public @Nullable TaskDTO findOneTask(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                                         @Nullable @WebParam(name = "taskDTO", partName = "taskDTO") final String taskId) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        if (taskId == null) return null;
        return taskService.mapTaskToDto(taskService.findOne(taskId));
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<TaskDTO> findAllTaskByUserId(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        final Collection<TaskDTO> collection = new ArrayList<>();
        for (final Task task :
                taskService.findAllByUserId(session.getUserId())) {
            collection.add(taskService.mapTaskToDto(task));
        }
        return collection;
    }


    @Override
    @WebMethod
    public void removeAllTask(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        if (session.getUserId() == null) return;
        taskService.removeAll(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<TaskDTO> findAllTaskInOrderDateBegin(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        if (session.getUserId() == null) return null;
        final Collection<TaskDTO> collection = new ArrayList<>();
        for (final Task task :
                taskService.findAllInOrderDateBegin(session.getUserId())) {
            collection.add(taskService.mapTaskToDto(task));
        }
        return collection;
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<TaskDTO> findAllTaskInOrderDateEnd(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        if (session.getUserId() == null) return null;
        final Collection<TaskDTO> collection = new ArrayList<>();
        for (final Task task :
                taskService.findAllInOrderDateEnd(session.getUserId())) {
            collection.add(taskService.mapTaskToDto(task));
        }
        return collection;
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<TaskDTO> findAllTaskInOrderReadinessStatus(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        final Collection<TaskDTO> collection = new ArrayList<>();
        for (final Task task :
                taskService.findAllInOrderReadinessStatus(session.getUserId())) {
            collection.add(taskService.mapTaskToDto(task));
        }
        return collection;
    }

    @Override
    @WebMethod
    public @Nullable Collection<TaskDTO> findTaskByPartNameOrDescription(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                                                                         @Nullable @WebParam(name = "part", partName = "part") final String part) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        if (session.getUserId() == null || part == null || part.isEmpty()) return null;
        final Collection<TaskDTO> collection = new ArrayList<>();
        for (final Task task :
                taskService.findByPartNameOrDescription(session.getUserId(), part)) {
            collection.add(taskService.mapTaskToDto(task));
        }
        return collection;
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<TaskDTO> findAllTask(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) throws IllegalAccessException {
        if (!sessionService.isValid(session)) throw new IllegalAccessException("Ваша сессия невалидная");
        if(session.getRoleType()!=RoleType.ADMINISTRATOR) throw new IllegalArgumentException("Вам не доступна эта команда");
        final Collection<TaskDTO> collection = new ArrayList<>();
        for (final Task task :
                taskService.findAll()) {
            collection.add(taskService.mapTaskToDto(task));
        }
        return collection;
    }
}
