package ru.chistyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.utility.PasswordParser;

import java.util.Collection;
import java.util.List;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String> {


    @Nullable
    List<Task> findByUserId(@NotNull String userId);

    void deleteByUserId(@NotNull String userId);

    @Nullable
    List<Task> findByUserIdAndNameLikeOrDescriptionLikeIgnoreCase(@NotNull String userId,@NotNull String name, @Nullable String description);

    @Nullable
    List<Task> findByUserIdOrderByDateBeginTaskAsc(@NotNull String id);

    @Nullable
    List<Task> findByUserIdOrderByDateEndTaskAsc(@NotNull String id);

    @Nullable
    List<Task> findByUserIdOrderByReadinessStatusAsc(@NotNull String id);

    @Nullable
    List<Task> findByProjectId(@NotNull String projectId);

}
