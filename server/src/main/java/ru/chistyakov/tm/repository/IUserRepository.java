package ru.chistyakov.tm.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.chistyakov.tm.entity.User;

@Repository
public interface IUserRepository extends JpaRepository<User, String> {

    User findUserByLoginAndPassword(@Nullable String login, @Nullable String password);

    User findUserByLogin(@Nullable String login);

    @Query("update User u set u.login = :login ,u.password = :password where u.id = :id")
    int updateUser(@Param("id") String id, @Param("login") String login, @Param("password") String password);

}
