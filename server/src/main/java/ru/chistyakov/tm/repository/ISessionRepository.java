package ru.chistyakov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.chistyakov.tm.entity.Session;

@Repository
public interface ISessionRepository extends JpaRepository<Session, String> {

}
