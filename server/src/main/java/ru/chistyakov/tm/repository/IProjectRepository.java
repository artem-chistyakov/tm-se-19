package ru.chistyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.entity.Project;

import java.util.Collection;
import java.util.List;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String> {

    @NotNull
    List<Project> findByUserId(@NotNull String userId);

    @NotNull
    List<Project> findByUserIdAndNameLikeOrDescriptionLike(@NotNull String userId, @NotNull String name, @NotNull String description);

    void deleteByUserId(@NotNull String userID);

    @Nullable
    List<Project> findByUserIdOrderByDateBeginProjectAsc(@NotNull String id);

    @Nullable
    List<Project> findByUserIdOrderByDateEndProjectAsc(@NotNull String id);

    @Nullable
    List<Project> findByUserIdOrderByReadinessStatusAsc(@NotNull String id);
}
