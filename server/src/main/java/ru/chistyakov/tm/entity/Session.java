package ru.chistyakov.tm.entity;

import lombok.Data;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.enumerate.RoleType;

import javax.persistence.*;

@Data
@Entity
@Table(name = "sessions")
public class Session extends AbstractEntity{

    @Nullable
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",updatable = false,nullable = false)
    private User user;

    @NotNull
    private Long timestamp = System.currentTimeMillis();

    @Nullable
    private String signature;

    @Nullable
    @Enumerated(EnumType.STRING)
    @Column(name = "role",updatable = false,nullable = false)
    private RoleType roleType;
}

