package ru.chistyakov.tm.entity;

import lombok.Data;
import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.enumerate.RoleType;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "users")
public class User extends AbstractEntity {
    @NotNull
    @Column(name = "login", nullable = false,unique = true)
    private String login = "";

    @NotNull
    @Column(name = "password", nullable = false)
    private String password = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private RoleType roleType = RoleType.USUAL_USER;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "user",
            cascade = CascadeType.ALL)
    private List<Project> projects;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "user",
            cascade = CascadeType.ALL)
    private List<Task> tasks;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "user",
            cascade = CascadeType.ALL)
    private List<Session> sessions;
}

