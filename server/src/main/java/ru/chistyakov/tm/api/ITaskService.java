package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.chistyakov.tm.dto.TaskDTO;
import ru.chistyakov.tm.entity.Task;

import java.util.Collection;

public interface ITaskService {
    @Transactional
    Task merge(@Nullable Task task);

    @Transactional
    Task persist(@Nullable Task task);

    @Nullable Task findOne(@Nullable String id);

    @Nullable Collection<Task> findAllByUserId(@Nullable String userId);


    @Transactional
    void remove(@Nullable String id);

    @Transactional
    void removeAll(@Nullable String userId);

    @Nullable Collection<Task> findAllInOrderDateBegin(@Nullable String userId);

    @Nullable Collection<Task> findAllInOrderDateEnd(@Nullable String userId);

    @Nullable Collection<Task> findAllInOrderReadinessStatus(@Nullable String userId);

    @Nullable Collection<Task> findByPartNameOrDescription(@Nullable String userId, @Nullable String part);

    @Nullable Collection<Task> findAll();

    @NotNull Task mapDtoToTask(@NotNull TaskDTO taskDTO);

    @NotNull TaskDTO mapTaskToDto( @NotNull Task task);
}
