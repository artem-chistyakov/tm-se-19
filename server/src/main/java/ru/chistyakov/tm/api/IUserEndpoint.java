package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.SessionDTO;
import ru.chistyakov.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IUserEndpoint {

    @NotNull
    String getURL();


    @WebMethod
    boolean registryUser(@Nullable @WebParam(name = "userDTO", partName = "userDTO") final UserDTO userDTO);

    @WebMethod
    SessionDTO authorizeUser(@Nullable @WebParam(name = "login", partName = "login") String login,
                             @Nullable @WebParam(name = "password", partName = "password") String password);

    @WebMethod
    @Nullable boolean updateUser(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                                 @NotNull @WebParam(name = "userDTO", partName = "userDTO") final UserDTO userDTO) throws IllegalAccessException;


    @WebMethod
    @Nullable
    UserDTO findWithLoginPassword(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session,
                                  @Nullable @WebParam(name = "login", partName = "login") String login,
                                  @Nullable @WebParam(name = "password", partName = "password") String password) throws IllegalAccessException;

    @WebMethod
    @Nullable
    UserDTO findUserById(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session) throws IllegalAccessException;

    @WebMethod
    void deleteAccount(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                       @Nullable @WebParam(name = "userId", partName = "userId") final String userId) throws IllegalAccessException;

    @WebMethod
    @Nullable Collection<UserDTO> findAllUser(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session) throws IllegalAccessException;

    @WebMethod
    void serializateDomain(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session) throws IllegalAccessException;

    @WebMethod
    void deserializateDomain(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session) throws IllegalAccessException;

    @WebMethod
    void saveDomainJacksonXml(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session) throws IllegalAccessException;

    @WebMethod
    void loadDomainJacksonXml(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session) throws IllegalAccessException;

    @WebMethod
    void saveDomainJacksonJson(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session) throws IllegalAccessException;

    @WebMethod
    void loadDomainJacksonJson(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session) throws IllegalAccessException;
}
