package ru.chistyakov.tm.api;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.chistyakov.tm.dto.SessionDTO;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.enumerate.RoleType;

public interface ISessionService {
    void validate(@Nullable SessionDTO session) throws IllegalAccessException;

    boolean isValid(@Nullable SessionDTO session);

    @Transactional
    SessionDTO open(@Nullable String userId, @Nullable RoleType roleType);

    @Transactional
    boolean close(Session session);
}
