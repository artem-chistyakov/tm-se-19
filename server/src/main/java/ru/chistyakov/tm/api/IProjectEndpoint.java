package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.dto.SessionDTO;
import ru.chistyakov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IProjectEndpoint {

    @NotNull
    String getURL();

    @WebMethod
    boolean persistProject(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session,
                           @Nullable @WebParam(name = "project", partName = "project") ProjectDTO project) throws IllegalAccessException;

    @WebMethod
    boolean mergeProject(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session,
                         @Nullable @WebParam(name = "project", partName = "project") ProjectDTO project) throws IllegalAccessException;

    @WebMethod
    @Nullable ProjectDTO findOneProject(@Nullable final @WebParam(name = "session", partName = "session") SessionDTO session,
                                        @Nullable final @WebParam(name = "projectId", partName = "projectId") String projectId) throws IllegalAccessException;

    @WebMethod
    boolean removeProject(@Nullable final @WebParam(name = "session", partName = "session") SessionDTO session,
                          @Nullable final @WebParam(name = "projectId", partName = "projectId") String projectId) throws IllegalAccessException;

    @WebMethod
    boolean updateProject(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                          @Nullable @WebParam(name = "project", partName = "project") final ProjectDTO project) throws IllegalAccessException;

    @WebMethod
    @Nullable Collection<ProjectDTO> findAllProjectByUserId(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session) throws IllegalAccessException;

    @WebMethod
    @Nullable Collection<ProjectDTO> findAllProject(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session) throws IllegalAccessException;

    @WebMethod
    @Nullable Collection<ProjectDTO> findAllProjectInOrderDateBegin(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session) throws IllegalAccessException;

    @WebMethod
    @Nullable Collection<ProjectDTO> findAllProjectInOrderDateEnd(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session) throws IllegalAccessException;

    @WebMethod
    @Nullable Collection<ProjectDTO> findAllProjectInOrderReadinessStatus(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session) throws IllegalAccessException;

    @WebMethod
    @Nullable Collection<ProjectDTO> findByPartProjectNameOrDescription(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session,
                                                                        @Nullable @WebParam(name = "part", partName = "part") String part) throws IllegalAccessException;

    @WebMethod
    void removeAllProject(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session) throws IllegalAccessException;
}
