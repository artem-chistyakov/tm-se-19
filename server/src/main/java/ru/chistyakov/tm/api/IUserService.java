package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.chistyakov.tm.dto.UserDTO;
import ru.chistyakov.tm.entity.User;

import java.util.Collection;

public interface IUserService {
    @Nullable User authoriseUser(@Nullable String login, @Nullable String password);

    @Transactional
    boolean updateUser(@Nullable User user);

    @Transactional
    User registryUser(@Nullable User user);

    @Nullable User findById(String userId);

    @Nullable User findByLoginAndPassword(@NotNull  String login,@NotNull  String password);

    void deleteAccount(@Nullable String userId);

    @Nullable Collection<User> findAll();

    @NotNull User mapUserDtoToUser(@NotNull UserDTO userDTO);

    @NotNull UserDTO mapUserToUserDto(@NotNull  User user);
}
