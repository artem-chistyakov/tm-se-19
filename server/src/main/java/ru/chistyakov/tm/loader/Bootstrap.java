package ru.chistyakov.tm.loader;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.endpoint.ProjectEndpoint;
import ru.chistyakov.tm.endpoint.SessionEndpoint;
import ru.chistyakov.tm.endpoint.TaskEndpoint;
import ru.chistyakov.tm.endpoint.UserEndpoint;
import ru.chistyakov.tm.service.PropertiesService;

import javax.xml.ws.Endpoint;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private PropertiesService propertiesService;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;
    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;
    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;
    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    public void start() {
        propertiesService.init();
        Endpoint.publish(projectEndpoint.getURL(), projectEndpoint);
        Endpoint.publish(taskEndpoint.getURL(), taskEndpoint);
        Endpoint.publish(userEndpoint.getURL(), userEndpoint);
        Endpoint.publish(sessionEndpoint.getURL(), sessionEndpoint);
    }
}

