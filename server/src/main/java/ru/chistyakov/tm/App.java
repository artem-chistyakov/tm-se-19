package ru.chistyakov.tm;

import org.apache.log4j.BasicConfigurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.chistyakov.tm.loader.Bootstrap;

public final class App {


    public static void main(String[] args) {
        BasicConfigurator.configure();

        ApplicationContext context =
                new AnnotationConfigApplicationContext(AppContext.class);
        final Bootstrap bootstrap = (Bootstrap) context.getBean("bootstrap");
        bootstrap.start();
    }

}