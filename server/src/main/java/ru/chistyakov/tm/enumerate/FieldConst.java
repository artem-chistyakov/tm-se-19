package ru.chistyakov.tm.enumerate;

public enum FieldConst {

    ID("id"), NAME("name"), DESCRIPTION("description"), DATE_BEGIN("date_begin"), DATE_END("date_end"),
    USER_ID("user_id"), PROJECT_ID("project_id"), TASK_ID("task_id"),
    SIGNATURE("signature"), TIMESTAMP("timestamp"), LOGIN("login"), PASSWORD_HASH("password_hash"), ROLE("role");

    FieldConst(String name) {
    }

}
