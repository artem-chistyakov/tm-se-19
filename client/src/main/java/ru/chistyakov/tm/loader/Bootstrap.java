package ru.chistyakov.tm.loader;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.*;
import ru.chistyakov.tm.command.AbstractCommand;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Scanner;

@Component
public final class Bootstrap {

    @Nullable
    private  Map<String, AbstractCommand> commandMap ;
    @NotNull
    private Scanner scanner = new Scanner(System.in);
    @Nullable
    @Autowired
    private IProjectEndpoint projectEndpoint;
    @Nullable
    @Autowired
    private ITaskEndpoint taskEndpoint;
    @Nullable
    @Autowired
    private ISessionEndpoint sessionEndpoint;
    @Nullable
    @Autowired
    private IUserEndpoint userEndpoint;
    @Nullable
    private SessionDTO session = null;
    @Nullable
    @Autowired
    private ApplicationContext applicationContext;

    public void setSession(@Nullable SessionDTO session) {
        this.session = session;
    }

    @Nullable
    public SessionDTO getSession() {
        return session;
    }

    @NotNull
    public Scanner getScanner() {
        return scanner;
    }

    @Nullable
    public Map<String, AbstractCommand> getCommandMap() {
        return commandMap;
    }

    private void execute(@Nullable final AbstractCommand abstractCommand, @Nullable final SessionDTO session)
            throws Exception{
        if (abstractCommand == null) throw new UnsupportedOperationException("Вам не доступна данная команда.");
        final Collection<RoleType> roleTypeCollection = Arrays.asList(abstractCommand.getSupportedRoles());
        if (session == null) {
            if (!roleTypeCollection.contains(null))
                throw new UnsupportedOperationException("Вам не доступна данная команда.");
            abstractCommand.execute();
        } else {
            if (roleTypeCollection.contains(session.getRoleType())) {
                abstractCommand.execute();
            } else throw new UnsupportedOperationException("Вам не доступна данная команда.");
        }
    }


    private void init() {
        commandMap = applicationContext.getBeansOfType(AbstractCommand.class);
        }

    public void start() {
        init();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (true) {
            System.out.println("\n" + "Введите команду");
            final AbstractCommand abstractCommand = commandMap.get(scanner.nextLine());
            try {
                execute(abstractCommand, session);
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
        }
    }
}

