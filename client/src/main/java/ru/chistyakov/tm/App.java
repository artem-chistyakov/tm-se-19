package ru.chistyakov.tm;

import org.apache.log4j.BasicConfigurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.chistyakov.tm.loader.Bootstrap;


public final class App {

    public static void main(String[] args) {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        BasicConfigurator.configure();
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppContext.class);
        Bootstrap bootstrap = applicationContext.getBean("bootstrap",Bootstrap.class);
        bootstrap.start();
    }
}