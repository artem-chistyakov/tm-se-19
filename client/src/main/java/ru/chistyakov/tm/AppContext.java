package ru.chistyakov.tm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.chistyakov.tm.api.*;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.endpoint.ProjectEndpointService;
import ru.chistyakov.tm.endpoint.SessionEndpointService;
import ru.chistyakov.tm.endpoint.TaskEndpointService;
import ru.chistyakov.tm.endpoint.UserEndpointService;
import ru.chistyakov.tm.loader.Bootstrap;

import java.util.Map;
import java.util.Scanner;

@Configuration
@ComponentScan("ru.chistyakov.tm")
public class AppContext {
    @Bean
    public Scanner getScanner() {
        return new Scanner(System.in);
    }

    @Bean
    public IUserEndpoint getIUserEndpoint() {
        return new UserEndpointService().getUserEndpointPort();
    }

    @Bean
    public IProjectEndpoint getIProjectEndpoint() {
        return new ProjectEndpointService().getProjectEndpointPort();
    }

    @Bean
    public ITaskEndpoint getITaskEndpoint() {
        return new TaskEndpointService().getTaskEndpointPort();
    }

    @Bean
    public ISessionEndpoint getISessionEndpoint() {
        return new SessionEndpointService().getSessionEndpointPort();
    }

    @Bean
    @Autowired
    public Scanner getScanner(Bootstrap bootstrap) {
        return bootstrap.getScanner();
    }

    @Bean
    @Autowired
    public Map<String, AbstractCommand> getCommandMap(Bootstrap bootstrap) {
        return bootstrap.getCommandMap();
    }
}
