package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.ProjectDTO;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

@Component("prc")
public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Удаляет проект по id";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите id проекта");
        final String projectId = scanner.nextLine();
        if (projectEndpoint.removeProject(bootstrap.getSession(), projectId))
            System.out.println("Проект с таким id удален");
        else throw new IllegalArgumentException("Ошибка удаления проекта");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
