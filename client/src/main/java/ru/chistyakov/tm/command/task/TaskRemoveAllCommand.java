package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;
@Component("trac")
public final class TaskRemoveAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Удаление всех команд авторизованного пользователя";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        taskEndpoint.removeAllTask(bootstrap.getSession());
        System.out.println("Все задачи удалены");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
