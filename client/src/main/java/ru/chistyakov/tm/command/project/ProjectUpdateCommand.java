package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.ProjectDTO;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

@Component("puc")
public final class ProjectUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Обновляет проект";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите id проекта");
        final String projectId = scanner.nextLine();
        System.out.println("Введите название проекта");
        final String projectName = scanner.nextLine();
        System.out.println("Введите описание проекта");
        final String descriptionProject = scanner.nextLine();
        System.out.println("Введите дату начала проекта DD.MM.yyyy");
        final String dateBeginProject = scanner.nextLine();
        System.out.println("Введите дату окончания проекта DD.MM.yyyy");
        final String dateEndProject = scanner.nextLine();
        final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(projectId);
        projectDTO.setName(projectName);
        projectDTO.setDescription(descriptionProject);
        if (projectEndpoint.updateProject(bootstrap.getSession(), projectDTO))
            System.out.println("Проект обновлен");
        else throw new IllegalArgumentException("Ошибка обновления проекта");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
