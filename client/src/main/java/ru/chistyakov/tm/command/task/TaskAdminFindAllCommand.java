package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.TaskDTO;
import ru.chistyakov.tm.command.AbstractCommand;

@Component("tafac")
public final class TaskAdminFindAllCommand extends AbstractCommand {

    @Override
    public @NotNull String getDescription() {
        return "Выводит все задачи всех пользователей";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        for (final TaskDTO task : taskEndpoint.findAllTask(bootstrap.getSession()))
            System.out.println("Task{" +"\n"+
                    "userId  =  '" + task.getUserId() + '\'' +"\n"+
                    "projectId  =  '" + task.getProjectId() + '\'' +"\n"+
                    "id  =  '" + task.getId() + '\'' +"\n"+
                    "name  =  '" + task.getName() + '\'' +"\n"+
                    "readinessStatus  =  " + task.getReadinessStatus() +"\n"+
                    "description  =  '" + task.getDescription() + '\'' +"\n"+
                    "dateBeginTask  =  " + task.getDateBeginTask() +"\n"+
                    "dateEndTask  =  " + task.getDateEndTask() +"\n"+
                    '}');
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
