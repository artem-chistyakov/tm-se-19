package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.UserDTO;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.utility.PasswordParser;

import java.util.UUID;
@Component("urc")
public final class UserRegistrationCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "регистрация пользователя";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("введите имя");
        final String login = scanner.nextLine();
        System.out.println("введите пароль");
        final String password = scanner.nextLine();
        final UserDTO userDTO = new UserDTO();
        userDTO.setId(UUID.randomUUID().toString());
        userDTO.setLogin(login);
        userDTO.setPassword(PasswordParser.parse(password));
        userDTO.setRoleType(RoleType.USUAL_USER);
        if (userEndpoint.registryUser(userDTO))
            System.out.println("Пользователь зарегистрирован");
        else System.out.println("Ошибка регистрации пользователя");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{null};
    }
}
