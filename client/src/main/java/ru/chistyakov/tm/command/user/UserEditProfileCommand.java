package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.UserDTO;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.utility.PasswordParser;
@Component("uepc")
public final class UserEditProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Изменение профиля пользователя";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите новое имя");
        final String login = scanner.nextLine();
        System.out.println(" Введите новый пароль");
        final String password = scanner.nextLine();
        final UserDTO userDTO = new UserDTO();
        userDTO.setId(bootstrap.getSession().getUserId());
        userDTO.setLogin(login);
        userDTO.setPassword(PasswordParser.parse(password));
        userDTO.setRoleType(RoleType.USUAL_USER);
        if (userEndpoint.updateUser(bootstrap.getSession(), userDTO))
            System.out.println("Профиль успешно изменен");
        else System.out.println("Ошибка изменения профиля");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
