package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.ProjectDTO;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

import java.util.List;

@Component("pfac")

public final class ProjectFindAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Поиск всех проектов авторизованного пользователя";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        final List<ProjectDTO> projectCollection = projectEndpoint.findAllProjectByUserId(bootstrap.getSession());
        if (projectCollection.isEmpty()) throw new IllegalArgumentException("Проекты не найдены");
        for (final ProjectDTO project : projectCollection)
            System.out.println("Project{" +"\n"+
                    "userId = '" + project.getUserId() + '\'' +"\n"+
                    "id  = '" + project.getId() + '\'' +"\n"+
                    "name = '" + project.getName() + '\'' +"\n"+
                    "readinessStatus  =  " + project.getReadinessStatus() +"\n"+
                    "description  =  '" + project.getDescription() + '\'' +"\n"+
                    "dateBeginProject  =  " + project.getDateBeginProject() +"\n"+
                    "dateEndProject  =  " + project.getDateEndProject() +"\n"+
                    '}');
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
