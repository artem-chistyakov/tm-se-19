package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.ReadinessStatus;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.TaskDTO;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.utility.DateParser;
@Component("tuc")
public final class TaskUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Обновляет команду";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите id задачи");
        final String taskId = scanner.nextLine();
        System.out.println("Введите новое id проекта");
        final String projectId = scanner.nextLine();
        System.out.println("Введите новое название задачи");
        final String taskName = scanner.nextLine();
        System.out.println("Введите новое описание задачи");
        final String descriptionTask = scanner.nextLine();
        System.out.println("Введите новую дату начала задачи DD.MM.yyyy");
        final String dateBeginTask = scanner.nextLine();
        System.out.println("Введите новую дату окончания задачи DD.MM.yyyy");
        final String dateEndTask = scanner.nextLine();
        final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(taskId);
        taskDTO.setName(taskName);
        taskDTO.setDateBeginTask(DateParser.getXMLGregorianCalendar(dateBeginTask));
        taskDTO.setDateEndTask(DateParser.getXMLGregorianCalendar(dateEndTask));
        taskDTO.setDescription(descriptionTask);
        taskDTO.setProjectId(projectId);
        taskDTO.setUserId(bootstrap.getSession().getUserId());
        taskDTO.setReadinessStatus(ReadinessStatus.PLANNED);
//        if (taskEndpoint.updateTask(session, taskDTO))
            System.out.println("Задача обновлена успешно");
//        else throw new IllegalArgumentException("Ошибка обновления задачи");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
