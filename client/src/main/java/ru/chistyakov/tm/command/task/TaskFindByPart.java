package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.TaskDTO;
import ru.chistyakov.tm.command.AbstractCommand;

import java.util.Collection;

@Component("tfbp")
public final class TaskFindByPart extends AbstractCommand {

    @Override
    public @NotNull String getDescription() {
        return "ищет задачи по части названия или описания";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите часть названия или описания");
        String part = scanner.nextLine();
        final Collection<TaskDTO> taskCollection = taskEndpoint.findTaskByPartNameOrDescription(bootstrap.getSession(), part);
        if (taskCollection.isEmpty()) throw new IllegalArgumentException("Задачи не найдены");
        for (final TaskDTO task : taskCollection)
            System.out.println("Task{" +"\n"+
                    "userId  =  '" + task.getUserId() + '\'' +"\n"+
                    "projectId  =  '" + task.getProjectId() + '\'' +"\n"+
                    "id  =  '" + task.getId() + '\'' +"\n"+
                    "name  =  '" + task.getName() + '\'' +"\n"+
                    "readinessStatus  =  " + task.getReadinessStatus() +"\n"+
                    "description  =  '" + task.getDescription() + '\'' +"\n"+
                    "dateBeginTask  =  " + task.getDateBeginTask() +"\n"+
                    "dateEndTask  =  " + task.getDateEndTask() +"\n"+
                    '}');
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
