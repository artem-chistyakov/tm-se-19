package ru.chistyakov.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

@Component("about")
public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Информация о сборке приложения";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Версия приложения :" + Manifests.read("Version") + "\n" +
                "разработчик :" + Manifests.read("Developer") + "\n" +
                "build number :" + Manifests.read("BuildNumber"));
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{null, RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
