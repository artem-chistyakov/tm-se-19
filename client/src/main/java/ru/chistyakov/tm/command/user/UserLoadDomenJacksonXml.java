package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;
@Component("uldjx")
public class UserLoadDomenJacksonXml extends AbstractCommand {

    @Override
    public @NotNull String getDescription() {
        return "загрузка предметной области с использованием jackson в формате xml";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
       userEndpoint.loadDomainJacksonXml(bootstrap.getSession());
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
