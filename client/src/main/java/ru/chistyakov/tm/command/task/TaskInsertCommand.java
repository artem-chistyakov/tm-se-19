package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.ReadinessStatus;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.TaskDTO;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.utility.DateParser;

import java.util.UUID;
@Component("tic")
public final class TaskInsertCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Создает новую задачу";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите  id проекта");
        final String projectId = scanner.nextLine();
        System.out.println("Введите  название задачи");
        final String taskName = scanner.nextLine();
        System.out.println("Введите  описание задачи");
        final String descriptionTask = scanner.nextLine();
        System.out.println("Введите  дату начала задачи DD.MM.yyyy");
        final String dateBeginTask = scanner.nextLine();
        System.out.println("Введите  дату окончания задачи DD.MM.yyyy");
        final String dateEndTask = scanner.nextLine();
        final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(UUID.randomUUID().toString());
        taskDTO.setName(taskName);
        taskDTO.setDateBeginTask(DateParser.getXMLGregorianCalendar(dateBeginTask));
        taskDTO.setDateEndTask(DateParser.getXMLGregorianCalendar(dateEndTask));
        taskDTO.setDescription(descriptionTask);
        taskDTO.setProjectId(projectId);
        taskDTO.setUserId(bootstrap.getSession().getUserId());
        taskDTO.setReadinessStatus(ReadinessStatus.PLANNED);
        if (taskEndpoint.persistTask(bootstrap.getSession(), taskDTO))
            System.out.println("Новая задача создана");
        else throw new IllegalArgumentException("Ошибка создания задачи");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
