package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.TaskDTO;
import ru.chistyakov.tm.command.AbstractCommand;

import java.util.Collection;

@Component("tfac")
public final class TaskFindAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Найти все команды авторизованного пользователя";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        final Collection<TaskDTO> collection = taskEndpoint.findAllTaskByUserId(bootstrap.getSession());
        if (collection.isEmpty()) throw new IllegalArgumentException("Задачи не найдены");
        for (final TaskDTO task : collection)
            System.out.println("Task{" +"\n"+
                    "userId  =  '" + task.getUserId() + '\'' +"\n"+
                    "projectId  =  '" + task.getProjectId() + '\'' +"\n"+
                    "id  =  '" + task.getId() + '\'' +"\n"+
                    "name  =  '" + task.getName() + '\'' +"\n"+
                    "readinessStatus  =  " + task.getReadinessStatus() +"\n"+
                    "description  =  '" + task.getDescription() + '\'' +"\n"+
                    "dateBeginTask  =  " + task.getDateBeginTask() +"\n"+
                    "dateEndTask  =  " + task.getDateEndTask() +"\n"+
                    '}');
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
