package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

@Component("prac")
public final class ProjectRemoveAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Удаляет все проекты текущего пользователя";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        projectEndpoint.removeAllProject(bootstrap.getSession());
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
