package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

@Component("udac")
public class UserDeleteAccountCommand extends AbstractCommand {

    @Override
    public @NotNull String getDescription() {
        return "Удаление аккаунта пользователя";
    }

    @Override
    public void execute() throws Exception {
        try {
            final String userId = bootstrap.getSession().getUserId();
            userEndpoint.deleteAccount(bootstrap.getSession(), userId);
            bootstrap.setSession(null);
        } catch (Exception e) {
            System.out.println("Ошибка удаления аккаунта");
        }
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
