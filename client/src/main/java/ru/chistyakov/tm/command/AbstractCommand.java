package ru.chistyakov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.chistyakov.tm.api.*;
import ru.chistyakov.tm.loader.Bootstrap;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.lang.IllegalAccessException;
import java.util.Map;
import java.util.Scanner;


public abstract class AbstractCommand {

    @Autowired
    protected IUserEndpoint userEndpoint;
    @Autowired
    protected IProjectEndpoint projectEndpoint;
    @Autowired
    protected ITaskEndpoint taskEndpoint;
    @Autowired
    protected ISessionEndpoint sessionEndpoint;

    @Autowired
    protected Map<String, AbstractCommand> commandMap;

    @Autowired
    protected Scanner scanner;

    @Autowired
    protected Bootstrap bootstrap;

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

    @NotNull
    public abstract RoleType[] getSupportedRoles();
}
