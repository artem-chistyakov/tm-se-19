package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.ProjectDTO;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

@Component("pafac")
public final class ProjectAdminFindAllCommand extends AbstractCommand {

    @Override
    public @NotNull String getDescription() {
        return "Выводит все проекты всех пользователей";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        for (ProjectDTO project : projectEndpoint.findAllProject(bootstrap.getSession())) {
            System.out.println("Project{" +"\n"+
                    "userId = '" + project.getUserId() + '\'' +"\n"+
                    "id  = '" + project.getId() + '\'' +"\n"+
                    "name = '" + project.getName() + '\'' +"\n"+
                    "readinessStatus  =  " + project.getReadinessStatus() +"\n"+
                    "description  =  '" + project.getDescription() + '\'' +"\n"+
                    "dateBeginProject  =  " + project.getDateBeginProject() +"\n"+
                    "dateEndProject  =  " + project.getDateEndProject() +"\n"+
                    '}');
        }
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
