package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.UserDTO;
import ru.chistyakov.tm.command.AbstractCommand;

@Component("ulpc")
public class UserLookProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Просмотр профиля пользователя";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        final UserDTO userDTO = userEndpoint.findUserById(bootstrap.getSession());
        System.out.println("User{" +"\n"+
                "ID = '" + userDTO.getId() + '\'' +"\n"+
                "login = '" + userDTO.getLogin() + '\'' +"\n"+
                "role = '" + userDTO.getRoleType() + '\'' +"\n"+
                '}');
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};

    }
}
