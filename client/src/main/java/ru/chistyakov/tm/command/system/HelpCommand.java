package ru.chistyakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

@Component("help")
public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Выводит описание всех комманд";
    }

    @Override
    public void execute() throws Exception {
        for (final AbstractCommand abstractCommand : commandMap.values())
            System.out.println(abstractCommand.getDescription());
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{null, RoleType.ADMINISTRATOR, RoleType.USUAL_USER};
    }
}
