package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

import java.io.IOException;
@Component("usdjj")
public class UserSaveDomenJacksonJson extends AbstractCommand {

    @Override
    public @NotNull String getDescription() {
        return "Сохранение предметной области в формате json с помощью fasterxml";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
       userEndpoint.saveDomainJacksonJson(bootstrap.getSession());
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
