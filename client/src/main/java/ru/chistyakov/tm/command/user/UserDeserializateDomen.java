package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;
@Component("udd")
public class UserDeserializateDomen extends AbstractCommand {

    @Override
    public @NotNull String getDescription() {
        return "Загрузка предметной области с использованием десериализации";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
       userEndpoint.deserializateDomain(bootstrap.getSession());
    }


    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
