package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;
@Component("trc")
public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Удаление одной команды авторизованного пользователя";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите id задачи");
        final String taskId = scanner.nextLine();
        if (taskEndpoint.removeTask(bootstrap.getSession(), taskId))
            System.out.println("Задача удалена");
        else throw new IllegalArgumentException("Ошибка удаления задачи");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
