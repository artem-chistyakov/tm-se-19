package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

import java.io.IOException;
@Component("uldjj")
public class UserLoadDomenJacksonJson extends AbstractCommand {

    @Override
    public @NotNull String getDescription() {
        return "загрузка предметной области с использование jackson в формате json";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
       userEndpoint.loadDomainJacksonJson(bootstrap.getSession());
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
