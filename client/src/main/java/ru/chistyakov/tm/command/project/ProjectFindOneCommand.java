package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.ProjectDTO;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

@Component("pfoc")
public final class ProjectFindOneCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Поиск одного проекта по id у авторизованного пользователя";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите id проекта");
        final String projectId = scanner.nextLine();
        final ProjectDTO project = projectEndpoint.findOneProject(bootstrap.getSession(), projectId);
        if (project == null) throw new IllegalArgumentException("Проект не найден");
        else
             System.out.println("Project{" +"\n"+
                    "userId = '" + project.getUserId() + '\'' +"\n"+
                    "id  = '" + project.getId() + '\'' +"\n"+
                    "name = '" + project.getName() + '\'' +"\n"+
                    "readinessStatus  =  " + project.getReadinessStatus() +"\n"+
                    "description  =  '" + project.getDescription() + '\'' +"\n"+
                    "dateBeginProject  =  " + project.getDateBeginProject() +"\n"+
                    "dateEndProject  =  " + project.getDateEndProject() +"\n"+
                    '}');
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
