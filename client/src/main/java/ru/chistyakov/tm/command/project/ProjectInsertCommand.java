package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.ProjectDTO;
import ru.chistyakov.tm.api.ReadinessStatus;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.utility.DateParser;

import java.util.UUID;

@Component("pic")
public final class ProjectInsertCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Создание нового проекта";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите название проекта");
        final String nameProject = scanner.nextLine();
        System.out.println("Введите описание проекта");
        final String descriptionProject = scanner.nextLine();
        System.out.println("Введите дату начала проекта dd.MM.yyyy");
        final String dateBeginProject = scanner.nextLine();
        System.out.println("Введите дату окончания проекта dd.MM.yyyy");
        final String dateEndProject = scanner.nextLine();
        final ProjectDTO projectDTO = new ProjectDTO();
        System.out.println(projectDTO.getId());
        projectDTO.setId(UUID.randomUUID().toString());
        projectDTO.setName(nameProject);
        projectDTO.setDescription(descriptionProject);
        projectDTO.setDateBeginProject(DateParser.getXMLGregorianCalendar(dateBeginProject));
        projectDTO.setDateEndProject(DateParser.getXMLGregorianCalendar(dateEndProject));
        projectDTO.setUserId(bootstrap.getSession().getUserId());
        projectDTO.setReadinessStatus(ReadinessStatus.PLANNED);
        if (projectEndpoint.persistProject(bootstrap.getSession(),projectDTO))System.out.println("Новый проект успешно создан");
        else throw new IllegalArgumentException("Ошибка создания проекта");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
