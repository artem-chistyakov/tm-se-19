package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.SessionDTO;
import ru.chistyakov.tm.command.AbstractCommand;

@Component("uac")
public final class UserAuthorizationCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Авторизация пользователя";
    }

    @Override
    public void execute()  {
        System.out.println("введите имя");
        final String login = scanner.nextLine();
        System.out.println("введите пароль");
        final String password = scanner.nextLine();
        final SessionDTO session1 = userEndpoint.authorizeUser(login, password);
        if (session1 == null) System.out.println("Пользователя с таким логином и паролем не существует");
        else bootstrap.setSession(session1);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{null};
    }
}
