package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.UserDTO;
import ru.chistyakov.tm.command.AbstractCommand;
@Component("ufa")
public class UserFindAll extends AbstractCommand {

    @Override
    public @NotNull String getDescription() {
        return "Выводит все профили пользователей";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        for (final UserDTO user :userEndpoint.findAllUser(bootstrap.getSession()))
            System.out.println("User{" +
                    "login='" + user.getLogin() + '\'' +
                    ", password='" + user.getPassword() + '\'' +
                    ", roleType=" + user.getRoleType() +
                    ", id='" + user.getId() + '\'' +
                    '}');
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
