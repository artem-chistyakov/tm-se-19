package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.ProjectDTO;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

import java.util.Collection;

@Component("pfarsc")
public final class ProjectFindAllOrderReadinessStatusCommand extends AbstractCommand {

    @Override
    public @NotNull String getDescription() {
        return "выводит все проеты в порядке статуса готовности";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        Collection<ProjectDTO> projectCollection = projectEndpoint.findAllProjectInOrderReadinessStatus(bootstrap.getSession());
        if (projectCollection.isEmpty()) throw new IllegalArgumentException("Проекты не найдены");
        for (ProjectDTO project : projectCollection)
            System.out.println("Project{" +"\n"+
                    "userId = '" + project.getUserId() + '\'' +"\n"+
                    "id  = '" + project.getId() + '\'' +"\n"+
                    "name = '" + project.getName() + '\'' +"\n"+
                    "readinessStatus  =  " + project.getReadinessStatus() +"\n"+
                    "description  =  '" + project.getDescription() + '\'' +"\n"+
                    "dateBeginProject  =  " + project.getDateBeginProject() +"\n"+
                    "dateEndProject  =  " + project.getDateEndProject() +"\n"+
                    '}');
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR, RoleType.USUAL_USER};
    }
}
