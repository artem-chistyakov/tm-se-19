package ru.chistyakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

@Component("exit")
public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Выход из программы";
    }

    @Override
    public void execute() throws Exception {
        System.exit(0);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{null, RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
